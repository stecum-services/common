package be.stecum.services.common.base.jpa;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

/**
 *
 * @author lucs
 */
@MappedSuperclass
public class BaseEntity {

    private String id = UUID.randomUUID().toString();

    @Column(name = "creation_date", nullable = false, updatable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate = new Date();

    @Column(name = "updated_date", nullable = false, updatable = true)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updatedDate = new Date();

    @PreUpdate
    public void onPreUpdate() {
        updatedDate = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getCreationDateTime() {
        return ZonedDateTime.ofInstant(creationDate.toInstant(), ZoneId.systemDefault());
    }

    public ZonedDateTime getUpdatedDateTime() {
        return ZonedDateTime.ofInstant(updatedDate.toInstant(), ZoneId.systemDefault());
    }

    
}
